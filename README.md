# CookBook Python 3 

![woman cooking](img/womancooking.png)![salada](img/greensalad.png)![soup](img/food.png)![espaguete](img/spaguet.png)![vinho](img/wineglass.png)

Cookbook significa **Livro de Receitas**. Aqui eu coleciono trechos de códigos em Python que creio que possam ser úteis. Bom proveito! 

---

- [cb0000:](cb0000_ordena_lista_composta) Ordenando lista composta.

- [cb0001:](cb0001_string_formatada.py) String retorna em caixa alta com espaçamento de 50 caracteres, com linha pontilhada em baixo e em cima.

- [cb0002:](cb0002_ler_apenas_letras.py) Função que verifica se a entrada é constituída apenas de letras.

- [cb0003:](cb0003_validar_num_inteiro.py) Validando número inteiro.

- [cb0004:](cb0004_validar_divisor.py) Validando divisor.

- [cb0005:](cb0005_equacao_2_grau/cb0005_equacao_2_grau.md) Cálculo das raízes de uma equação do 2º grau, dado as entradas de A, B e C ou encontrar a equação, dadas as raízes.

- [cb0006:](cb0006_tipo_triangulo.py) Função que verifica e identifica o tipo de triângulo.

- [cb0007:](cb0007_matriz/cb0007_matriz.md) Criação e operações com matrizes.

- [cb0008:](cb0008_fatorial.py) Fatorial não recursivo de um número.

- [cb0009:](cb0009_fibonacci.py) Sequência Fibonacci.

- [cb0010:](cb0010_progressao_aritmetica.py) Calculando o enésimo termo de uma progressão aritmética.

