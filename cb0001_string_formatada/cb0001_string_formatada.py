#criando a função
def ler_cabecalho(msg: str) -> None:
    '''
    recebe uma string e retorna a mesma em caixa alta com espaçamento de 50 caracteres, incluindo a string passada.
    linha pontilhada em baixo e em cima.
    '''
    #print() imprime uma mensagem na tela
    print('-'*50)
    print(f'{msg.upper():^50}')
    print('-'*50)


if __name__ == '__main__':
    #chamando função
    ler_cabecalho('sua mensagem aqui')
