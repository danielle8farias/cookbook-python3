# Equação do 2º grau


- [encontrar a equação do 2º grau através dos valores das raízes;](cb0005_encontrar_equacao_2_grau.py)

- [encontrar as raízes da equação informando os valores de a, b e c.](cb0005_raizes_equacao_2_grau.py)
