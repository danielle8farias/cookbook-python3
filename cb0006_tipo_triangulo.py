#função que verifica e identifica o triângulo
#   arugmento da função são os lados do triângulo
def verificar_triangulo(lado1: float, lado2: float, lado3: float) -> str:
    #verificando se a soma de dois lados quaisquer é maior do que o terceiro lado
    if lado2+lado3 > lado1 and lado1+lado3 > lado2 and lado1+lado2 > lado3:
        #verificando se possui os 3 lados iguais
        if lado1 == lado2 and lado2 == lado3:
            #função print retorna uma string na tela
            print('Triângulo Equilátero: três lados iguais.')
        #verificando se possui 2 lados iguais
        elif lado1 == lado2 or lado1 == lado3 or lado2 == lado3:
            print('Triângulo Isósceles: quaisquer dois lados iguais.')
        else:
            print('Triângulo Escaleno: três lados diferentes.')
    else:
        print('Esses valores não formam um triângulo.')


if __name__ == '__main__':
    #chamadas da função
    #   lendo a função com as entradas
    verificar_triangulo(8,8,8)
    verificar_triangulo(4,5,4)
    verificar_triangulo(3,4,5)
    verificar_triangulo(2,5,9)
