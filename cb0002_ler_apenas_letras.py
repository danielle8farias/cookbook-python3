#criando a função
def ler_palavra(msg: str) -> str:
    '''
    Verifica se o que foi digitado foram apenas letras
    '''
    try:
        #strip() retira espaços em branco no início e fim da string
        #upper() converte para letras maiúsculas
        #variável 'palavra' recebe a string tratada
        palavra = msg.upper().strip()
        #retirando espaços
        #split() cria uma lista onde a separação dos itens é feita através de um determinado caractere; por padrão é o espaço
        palavras = palavra.split()
        #unindo a string sem espaços
        #join une uma lista de strings em uma nova string; o item antes do join pode ser preenchido por qualquer caractere, inclusive nenhum
        palavras = ''.join(palavras)
        #isalpha() se possui apenas letras
        #verificando se palavra possui caracteres que não sejam letras
        if not palavras.isalpha():
            #criando exceção, caso expressão acima seja verdadeira
            raise Exception('Digite apenas letras.')
    #dispara exceção criada
    except Exception as erro:
        #print() imprime uma string formatada
        #   {variável entre chaves}
        return f'Valor inválido: {erro}'
    #se o 'try' for válido retorna a própria palavra
    else:
        return palavra


if __name__ == '__main__':
    #chamadas da função    
    print(ler_palavra('25')) #Lendo string 25 como entrada da função
    print(ler_palavra('vinte e 6'))
    print(ler_palavra('vinte e sete'))
