#importando módulo sleep da biblioteca time
from time import sleep

#criando função que calcula o fatorial
#   recebe como argumento uma variável inteira (número natural)
def fatorial(num: int) -> int:
    #o menor fatorial é 1
    #   1! = 1
    #   0! = 1
    f = 1
    #laço while
    #enquanto o número passado for maior do que zero
    while num > 0:
        #se o número for maior do que 1
        if num > 1:
            #print() imprime na tela uma string formatada
            #   f-string
            #   {variável dentro das chaves}
            #   end='' para não pular de linha após o print
            #   flush para evitar o buffer do sleep
            print(f'{num} x ', end='', flush=True)
            #aguarda 0,5 segundos a cada print do laço
            sleep(0.5)
        #se o número for igual a 1
        else:
            print(f'{num}', flush=True)
            sleep(0.5)
        # f = f * num
        #calculando o fatorial de modo iterativo (não recursivo)
        f *= num
        # num = num -1
        #   subtraindo -1 a cada iteração, pois f(n) = n * (n-1) * (n-2) e assim em diante
        num -= 1
    #retorna o resultado do fatorial
    return f


if __name__ == '__main__':
    #chamadas da função
    fatorial(1)
    print(fatorial(5))
    print(fatorial(8))
