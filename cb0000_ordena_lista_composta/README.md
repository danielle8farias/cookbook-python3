# Receita: Ordenando listas compostas

---

## 1 - Usando uma função:

Vamos supor que eu tenho uma **lista composta**, no seguinte modelo:

```py
lista_precos = [['lápis', 1.5], ['caneta', 2], ['caderno', 25.9], ['borracha', 1.2], ['estojo', 14.85] ]
```

É uma lista composta com preços de material escolar, onde a primeira posição é o nome do item e a segunda, o seu preço.

Como podemos ver, é uma lista com 5 posições e as sublistas com 2 posições.

![desenho da lista](img/lista_precos.png)

- **lista_precos** é o nome da variável que armazena a nossa lista.
- **=** o sinal de igual representa a atribuição de um valor (ou objeto) que está a direita, a uma variável que está a esquerda.
- **[ ]** os colchetes são a representação da lista, onde os objetos são separados pela vírgula.
- **[ [ ], [ ] ]** sublistas são representadas por colchetes dentro de colchetes, separados por vírgulas quando há  mais de uma sublista.
- **' '** as aspas simples são usadas para indicar uma sequência de caracteres *(string)*.
- temos valores do tipo inteiro *(int)* como o valor da caneta (2);
- e valores do tipo real, ou ponto flutuante *(float)*, como o valor da lápis (1.5) onde é usado o ponto no lugar da vírgula.
- os **índices** da lista são ordenados por números inteiros de 0 até 4, no nosso caso. Sendo **zero** o **1º elemento**, **1** o **2º elemento** e assim por diante.

Sendo assim, se quisermos acessar o 3º item da lista (caderno e o seu preço), por exemplo, fazemos:

```py
lista_precos[2]
```

Teremos como retorno:

```py
['caderno', 25.9]
```

Se quisermos apenas o nome do objeto, fazemos:

```py
>>> lista_precos[2][0]
'caderno'
```

Obtendo acesso ao item 2 da lista e 0 da sublista.

![acessando nome do objeto do 3º item](img/item_2_0.png)

Agora, para fazer a ordenação dessa lista, primeiro precisamos criar a nossa função. Assim:

```py
def ordenar():
```

- **def** é a palavra-chave do Python para definirmos uma função.
- **ordenar** foi o nome que eu escolhi para dar à minha função. Você pode dar o nome que achar mais adequado.
- **( )** os parênteses servem para que possamos colocar os parâmetros/argumentos da nossa função.
- **:** os dois pontos indicam o fim do cabeçalho da nossa função.

Como o que eu quero ordenar é uma lista composta, mas ainda assim uma lista, vou indicar para a minha função o argumento desejado:

```py
def ordenar(lista):
```

O nome do meu argumento será a variável usada dentro da função que acabamos de criar.

Agora, eu vou adicionar as anotações de funções *(Function Annotations)*, que é opcional, mas ajuda a documentar a sua função indicando quais são os tipos esperados por ela.

```py
def ordenar(lista: list) -> list:
```

- **lista: list** indica que espero que o valor do meu parâmetro seja do tipo lista *(list)*;
- **-> list** indica que o retorno da minha função também será do tipo lista.

Agora vamos dizer qual será o retorno da função que desejamos:

```py
def ordenar(lista: list) -> list:
    return lista[1]
```

- Aqui é importante notar a **indentação** (recuo a direita). O Python usa desse recurso para montar um tipo de hierarquia; para entender que os comandos abaixo, que seguem esse padrão, pertencem à função acima.
- **return** indica o retorno da nossa função.
- **lista** é a nossa variável que queremos de volta, porém vamos fazer uma pequena modificação nela.
- **[1]** indica que queremos a segunda posição da nossa lista.

Como queremos ordenar pelo preço, vamos pegar o segundo item da nossa sublista.

![acessando pelo preço](img/acesso_precos.png)

Se usarmos somente essa função diretamente na nossa lista, ela nos retornará apenas o 2º item (caneta e seu preço), mas vamos **combiná-la com outra função** para fazer a ordenação que queremos.

A outra função é uma função pronta para uso *(built-in)*, chamada de **sort**. Ela faz a ordenação de listas simples (não compostas) *in-place*, isto é, usando a própria lista, sem a necessidade de criar uma nova variável para o armazenamento do seu retorno.

Um dos argumentos da função sort, é a chave **(key)** que por padrão vem com o valor **None**, ou seja, não possui nenhum valor associado a ela. Essa chave é usada pelo sort como base para a classificação de todo processo da ordenação. Assim, o valor padrão None significa que os itens da lista serão classificados diretamente sem levar em conta um valor de chave.

Então fazemos:

```py
lista_precos.sort(key=ordenar)
```

Estamos passando como a **chave** do **sort** a nossa **função ordenar**. Isso fará com que o sort use como base o 2º elemento de cada sublista. Fazendo assim a ordenação pelo preço. Ele irá comparar cada um dos preços e retornar a mesma lista, porém modificada (ordenada).

Caso quiséssemos fazer a ordenação pelo nome de cada objeto, bastava mudar o retorno da nossa função para pegar a primeira posição da nossa sublista.

```py
def ordenar(lista: list) -> list:
    return lista[0]
```

Também é possível fazer a ordenação em ordem decrescente.

Por padrão a função **sort** vem o argumento **reverse** marcado como **False**. Ou seja, o valor booleano *Falso* mantém algo como desativado. Para ativá-lo basta mudar o seu valor para verdadeiro, assim:

```py
lista_precos.sort(key=ordenar, reverse=True)
```

Agora a lista ficou ordenada do maior para o menor valor dos objetos. Basta usar a função print novamente para verificar a nova ordenação na tela.

Vamos criar mais duas funções. Uma para retornar o valor em ordem crescente e outra para decrescente.

```py
def ordenar_preco_crescente(lista: list) -> list:
    lista_precos.sort(key=ordenar)
    return lista_precos
```

```py
def ordenar_preco_decrescente(lista: list) -> list:
    lista_precos.sort(key=ordenar, reverse=True)
    return lista_precos
```

Perceba que ambas retornam a mesma variável de entrada, mas seu valor não é mais o mesmo. Foi modificado.

Agora precisamos ver esse resultado na tela e para isso usamos a função **print**.

```py
print(ordenar_preco_crescente(lista_precos))
```

- **print** imprime na tela, no formato de string, valor ou objeto entre parênteses.
- **ordenar_preco_crescente** é a chamada da nossa função criada acima, para fazer a ordenação do menor para o maior valor.
- **lista_precos** é o argumento da nossa função; a nossa variável.

![lista ordenada pelo preço](img/lista_precos_ordenada.png)

O processo pode ser repetido para a função de ordenação decrescente.

```py
print(ordenar_preco_decrescente(lista_precos))
```

Para usar o exemplo dessa função e ainda assim permitir que esse script seja usado como um **módulo**, vou acrescentar a variável ```__name__```, junto ao comando condicional **if**:

```py
if __name__ == '__main__':
```

- **módulos** são conjuntos de códigos que executam determinadas funções. No nosso caso, retorna o valor de uma lista composta ordenada.
- **if** é uma estrutura de condição que vai avaliar uma expressão lógica como verdadeiro ou falso. Caso seja avaliado como verdadeiro, o comando dentro do *if* é executado.
- **name** essa variável verifica se as funções presentes no script estão sendo exportadas ou executadas como arquivo principal.
- **==** é o sinal que faz a comparação de igualdade.
- **main** significa principal. 

O que esse trecho de código quer dizer é: se a variável *name* for igual a *main*, ou seja, se esse arquivo estiver sendo executado diretamente, faça (alguma coisa).

E então acrescentamos a nossa lista com seus valores e as funções print, com as chamadas das funções que criamos:

```py
if __name__ == '__main__':
    lista_precos = [['lápis', 1.5], ['caneta', 2], ['caderno', 25.9], ['borracha', 1.2], ['estojo', 14.85] ]
    print(ordenar_preco_crescente(lista_precos))
    print(ordenar_preco_decrescente(lista_precos))
```

- Perceba que assim como a função, aqui também temos a **indentação** (recuo a direita). Indicando os comandos que estão dentro do *if*.

[Veja como ficou o script de ordenação de lista composta usando função.](ordena_lista_composta_funcao.py)


---

## 2 - Usando a expressão lambda:

Também é possível fazer a **ordenação** de uma **lista composta** com o auxílio da **expressão lambda**.

**Expressões lambda** são funções anônimas do Python. Elas funcionam do seguinte modo: 

```py
lambda argumentos_da_função: retorno_da_função
```

De modo geral, são úteis para funções mais simples ou mais otimizadas.

Para o nosso caso, basta fazer com que o retorno seja o mesmo da função que criamos na parte 1 (acima):

```py
lambda lista: lista[1]
```

- **lambda** a invocação da nossa função anônima.
- **lista** é o argumento da nossa função.
- **lista[1]** é o retorno da nossa função.

tags: def, python, função, lista, ordenação, lambda
