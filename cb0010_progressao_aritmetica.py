#importando módulo sleep da biblioteca time
from time import sleep

#criando a função que calcula a progressão aritmética
#   argumentos da função são
#   A1 primeiro termo; r razão
def pa_10termos(A1: float, r: float) -> float:
    #variável de controle
    i = 1
    #inicialmemte o último termo é igual ao primeiro
    An = A1
    #inicialmente será mostrado os 10 primeiros termos
    termo = 10
    #variável de controle dos termos da PA
    total_termos = 10
    #laço while
    #   enquanto o termo for diferente de zero
    #   zero é a condição de parada do laço
    while termo != 0:
        #outro laço
        #   enquanto o termo for maior do que zero
        while termo > 0:
            #print() retorna uma string formatada na tela
            #   {variável dentro das chaves}
            #   f-strings
            #   :.2f até duas casas decimais do tipo float
            #   ao final do print temos a string ' -> ' ao invés da quebra de linha padrão
            #   flush para evitar o buffer do sleep
            print(f'{An:.2f}', end=' -> ', flush=True)
            #aguarda 0,5 segundos a cada print do laço
            sleep(0.5)
            #fórmula do cálculo do enésimo termo de uma PA
            An = A1 + i*r
            #incrementando o número de termos
            #   i = i + 1
            i += 1
            #decrementando o número de termos inicial/dado
            # termo = termo - 1
            termo -= 1
        print('pausa')
        print('Digite 0 para encerrar.')
        #int() converte para número inteiro a string recebida
        #input() captura o que foi digitado no teclado
        termo = int(input('Quantos termos deseja mostrar? '))
        #atualizando total de termos
        #   total_termos = total_termos + termo
        total_termos += termo
    #imprimendo o retorno da função
    print(f'Total de termos mostrados: {total_termos}\n')
    #função retorna o último termo da PA e o total de termos
    return An, total_termos


if __name__ == '__main__':
    #chamadas das funções
    pa_10termos(5,5)
    pa_10termos(0,3.1415)
    pa_10termos(100,-3)
