#criando a função que efetua a multiplicação de matrizes
#   função recebe 2 argumentos
#   os argumentos são duas listas
def multi_matriz(A, B):
    #duas variáveis recebem dois valores ao mesmo tempo
    #   'len(A)' tamanho da 1ª lista é o número de linhas
    #   'len(A[0])' tamanho da 1ª linha da matriz A é o número de colunas
    num_linhas_A, num_colunas_A = len(A), len(A[0])
    num_colunas_B, num_colunas_B = len(B), len(B[0])
    #criando lista vazia onde ficará a matriz resultado da multiplicação
    multiplicacao = list()
    #laço for
    #   para cada item até o número de linhas da matriz A
    #   i começa em zero e vai até (num_linhas_A - 1)
    #   percorre número de linhas de A
    for i in range(num_linhas_A):
        #lista vazia que será o número de linhas da matriz resultado da multiplicação
        linha = []
        #percorre número de colunas de B
        for j in range(num_colunas_B):
            soma = 0
            #percorre número de colunas de A
            for k in range(num_colunas_A):
                #regra da multiplicação de matrizes
                #   cada item da linha de A multiplica cada item da coluna de B
                #   o resultado da soma será cada item da nova matriz resultado da multiplicação
                soma += A[i][k] * B[k][j]
            #adicionando ao final da lista o resultado de cada soma
            linha.append(soma)
        #adicionando as linhas à nova matriz
        multiplicacao.append(linha)
    #print() imprime da tela o retorno da função
    print(multiplicacao)
    #retorna a nova lista
    return multiplicacao


if __name__ == '__main__':
    #chamada da função
    #número de colunas da matriz A deve ser igual ao número de linhas da matriz B
    #   matriz A=[[1,2],[3,4]] 2 linha e 2 colunas
    #   matriz B=[[4,3],[2,1]] 2 linhas e 2 colunas
    multi_matriz([[1,2],[3,4]], [[4,3],[2,1]]) #resultado deve ser uma matriz 2x2
    #matriz A=[[3],[2]] 2 linhas e 1 coluna
    #matriz B=[[1,6,5]] 1 linha e 3 colunas
    multi_matriz([[3],[2]], [[1,6,5]]) #resultado deve ser 2x3
    #matriz A=[[2,3],[4,6]] 2 linhas e 2 colunas
    #matriz B=[[1,3,0],[2,1,1]] 2 linhas e 3 colunas
    multi_matriz([[2,3],[4,6]], [[1,3,0],[2,1,1]]) #resultado deve ser 2x3
