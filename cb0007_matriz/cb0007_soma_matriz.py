#criando função que efetua a soma de duas matrizes
#   para somar as matrizes elas devem ser de mesma ordem
def somar_matrizes (A, B):
    #len() pega o tamanho da lista (matriz) A
    #   número de linhas é o comprimento da matriz
    num_linhas = len(A)
    #número de colunas é o tamanho de uma das sublistas de matriz    
    num_colunas = len(A[0])
    #criando lista vazia que receberá o resultado da soma
    soma = []
    #laço for
    #   para cada item até o número de linhas da matriz
    #   range() vai de zero até (num_linhas -1)
    #   percorrendo as linhas da matriz
    for i in range(num_linhas):
        #criando a lista para as linhas
        linha = []
        #percorrendo as colunas da matriz
        for j in range(num_colunas):
            #variável recebe o valor da soma de cada um dos itens das duas matrizes
            valor_soma = (A[i][j] + B[i][j])
            #adiciona ao final da lista linha, cada valor
            linha.append(valor_soma)
        #adicionando ao final da lista, cada linha
        soma.append(linha)
    #print() imprime da tela o retorno da função
    print(soma)
    #retorna a nova matriz
    return soma


if __name__ == '__main__':
    #chamada da função
    #   matriz A = [[1,2],[3,4]]
    #   matriz B = [[4,3],[2,1]]
    somar_matrizes([[1,2],[3,4]], [[4,3],[2,1]])
    somar_matrizes([[1,2,3]], [[4,5,6]])
