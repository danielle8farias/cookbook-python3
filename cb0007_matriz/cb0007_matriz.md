# Matrizes


- [criando uma matriz;](cb0007_construcao_matriz.py)

- [multiplicação de duas matrizes;](cb0007_multip_matrizes.py)

- [multiplicação de uma matriz por um número real;](cb0007_multip_real_matriz.py)

- [soma de duas matrizes;](cb0007_soma_matriz.py)

- [subtração de duas matrizes.](cb0007_subtra_matriz.py)
