#importando módulo sleep da biblioteca time
from time import sleep

#criando função que calcula a sequência Fibonacci
#   o argumento deve ser um número inteiro maior que zero
def fibo(num):
    #condicional
    #   se o número passado for igual a 1
    if num == 1:
        #print() imprime uma string formatada na tela
        print('0')
        #retornando zero
        return 0
    #else if
    #caso o número seja maior do que zero (não sendo 1, que já foi testado acima)
    elif num > 0:
        #o 1º elemento da sequência é zero
        t1 = 0
        #o 2º elemento da sequência é um
        t2 = tn = 1
        #imprimindo os dois primeiros elementos da sequência
        #   end='' serve para não pular a linha após o print
        #   flush para evitar o buffer do sleep
        print(f'{t1} -> {t2}', end='', flush=True)
        #aguarda 0,5 segundos a cada print do laço
        sleep(0.5)
        #variável de controle
        #   para que seja contado a partir do 3º elemento da sequência
        i = 3
        #laço while
        #   enquanto a variável de controle for menor do que o número passado
        while i <= num:
            #somando os dois termos anteriores para saber o seguinte
            tn = t1 + t2
            #{variável dentro das chaves}
            print(f' -> {tn}', end='', flush=True)
            sleep(0.5)
            #jeito pythonico de fazer a troca <3
            #   t1 = t2
            #   t2 = tn
            t1, t2 = t2, tn
            # i = i + 1
            #   incrementando a variável de controle
            i += 1
    print()


if __name__ == '__main__':
    fibo(8)
    fibo(1)
    fibo(2)
    fibo(4)
